/*
 Navicat Premium Data Transfer

 Source Server         : DATABASE
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : mpi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 22/02/2023 05:14:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carts
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `id_product` int NOT NULL,
  `id_payment` int NULL DEFAULT NULL,
  `amount_product` int NOT NULL,
  `total_product` int NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `variant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carts
-- ----------------------------
INSERT INTO `carts` VALUES (1, 2, 2, NULL, 1, 1, 'XXS', '_HITAM', '2023-02-22 04:16:56', '2023-02-22 04:27:53');
INSERT INTO `carts` VALUES (3, 2, 1, NULL, 1, 1, '_L', '_BIRU_UNGU', '2023-02-22 04:29:37', '2023-02-22 04:29:43');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'PAKAIAN MOESLIM', 'pakaian_moeslim', 'http://127.0.0.1:8001/assets/upload/category/NxhL1B-1676827367.png', 10, '2023-02-20 00:03:32', '2023-02-20 00:22:47');
INSERT INTO `categories` VALUES (2, 'PAKAIAN WANITA', 'pakaian_wanita', 'http://127.0.0.1:8001/assets/upload/category/7UBs0y-1676826264.png', 10, '2023-02-20 00:04:24', '2023-02-20 00:04:24');

-- ----------------------------
-- Table structure for dymantic_instagram_basic_profiles
-- ----------------------------
DROP TABLE IF EXISTS `dymantic_instagram_basic_profiles`;
CREATE TABLE `dymantic_instagram_basic_profiles`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dymantic_instagram_basic_profiles_username_unique`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dymantic_instagram_basic_profiles
-- ----------------------------

-- ----------------------------
-- Table structure for dymantic_instagram_feed_tokens
-- ----------------------------
DROP TABLE IF EXISTS `dymantic_instagram_feed_tokens`;
CREATE TABLE `dymantic_instagram_feed_tokens`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int UNSIGNED NOT NULL,
  `access_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_profile_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dymantic_instagram_feed_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for invoices
-- ----------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `no_inv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `id_transaction` int NOT NULL,
  `id_product` int NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of invoices
-- ----------------------------
INSERT INTO `invoices` VALUES (1, 'INV-dCmea8', 2, 1, 1, 'pending', '2023-02-22 04:18:28', '2023-02-22 04:18:28');
INSERT INTO `invoices` VALUES (2, 'INV-5uFXky', 2, 2, 1, 'pending', '2023-02-22 04:19:36', '2023-02-22 04:19:36');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_reset_tokens_table', 1);
INSERT INTO `migrations` VALUES (3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (6, '2020_05_21_100000_create_teams_table', 1);
INSERT INTO `migrations` VALUES (7, '2020_05_21_200000_create_team_user_table', 1);
INSERT INTO `migrations` VALUES (8, '2020_05_21_300000_create_team_invitations_table', 1);
INSERT INTO `migrations` VALUES (9, '2023_02_15_104536_create_payment_methods_table', 1);
INSERT INTO `migrations` VALUES (10, '2023_02_15_104600_create_transactions_table', 1);
INSERT INTO `migrations` VALUES (11, '2023_02_15_104610_create_transaction_details_table', 1);
INSERT INTO `migrations` VALUES (12, '2023_02_15_104755_create_invoices_table', 1);
INSERT INTO `migrations` VALUES (13, '2023_02_15_104833_create_products_table', 1);
INSERT INTO `migrations` VALUES (14, '2023_02_15_104857_create_product_details_table', 1);
INSERT INTO `migrations` VALUES (15, '2023_02_15_110643_create_carts_table', 1);
INSERT INTO `migrations` VALUES (16, '2023_02_15_110703_create_va_users_table', 1);
INSERT INTO `migrations` VALUES (17, '2023_02_15_121510_create_categories_table', 1);
INSERT INTO `migrations` VALUES (18, '2023_02_15_121519_create_sub_categories_table', 1);
INSERT INTO `migrations` VALUES (19, '2023_02_15_193028_create_sessions_table', 1);
INSERT INTO `migrations` VALUES (20, '2023_02_15_193428_create_reviews_table', 1);
INSERT INTO `migrations` VALUES (21, '2023_02_15_200632_create_instagram_basic_profile_table', 1);
INSERT INTO `migrations` VALUES (22, '2023_02_15_200632_create_instagram_feed_token_table', 1);
INSERT INTO `migrations` VALUES (23, '2023_02_15_222131_create_stores_table', 1);
INSERT INTO `migrations` VALUES (24, '2023_02_15_222426_create_store_details_table', 1);
INSERT INTO `migrations` VALUES (25, '2023_02_17_135454_create_password_resets_table', 2);
INSERT INTO `migrations` VALUES (26, '2023_02_17_173055_create_subscribes_table', 3);
INSERT INTO `migrations` VALUES (27, '2023_02_17_173245_create_promos_table', 3);
INSERT INTO `migrations` VALUES (28, '2023_02_20_011158_create_wishlists_table', 4);
INSERT INTO `migrations` VALUES (29, '2023_02_20_013635_create_size_products_table', 4);
INSERT INTO `migrations` VALUES (30, '2023_02_20_013656_create_variant_products_table', 4);

-- ----------------------------
-- Table structure for password_reset_tokens
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_tokens`;
CREATE TABLE `password_reset_tokens`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_reset_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES (1, 'ibansyahdienx7@gmail.com', '965fdf0b328f986727506d41e9119f4a', '2023-02-17 14:03:53', '2023-02-17 14:03:53');

-- ----------------------------
-- Table structure for payment_methods
-- ----------------------------
DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE `payment_methods`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_methods
-- ----------------------------
INSERT INTO `payment_methods` VALUES (1, 84849, 'GO PAY', 'http://127.0.0.1:8001/assets/upload/payment_method/OFK5Y2-1676855538.png', 'go_pay', 'gopay', '10', '2023-02-20 08:12:18', '2023-02-20 08:12:18');
INSERT INTO `payment_methods` VALUES (2, 73455, 'BANK BNI', 'http://127.0.0.1:8001/assets/upload/payment_method/EqVvVa-1676855622.png', 'bni', 'bank_transfer', '10', '2023-02-20 08:13:42', '2023-02-20 08:13:42');
INSERT INTO `payment_methods` VALUES (3, 63286, 'BANK PERMATA', 'http://127.0.0.1:8001/assets/upload/payment_method/8pb9gg-1676855673.png', 'permata', 'permata', '10', '2023-02-20 08:14:33', '2023-02-20 08:14:33');
INSERT INTO `payment_methods` VALUES (4, 3551, 'BANK MANDIRI', 'http://127.0.0.1:8001/assets/upload/payment_method/At7cIc-1676855862.png', 'bank_mandiri', 'echannel', '10', '2023-02-20 08:15:14', '2023-02-20 08:18:14');
INSERT INTO `payment_methods` VALUES (5, 34256, 'BANK BRI', 'http://127.0.0.1:8001/assets/upload/payment_method/UExMBx-1676893200.png', 'bri', 'bank_transfer', '10', '2023-02-20 18:40:00', '2023-02-20 18:40:00');

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token` ASC) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type` ASC, `tokenable_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for product_details
-- ----------------------------
DROP TABLE IF EXISTS `product_details`;
CREATE TABLE `product_details`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int NOT NULL,
  `tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_details
-- ----------------------------
INSERT INTO `product_details` VALUES (1, 1, '#pakaian muslim, #pakaian', 'LIHAT KOLEKSI BARANG KITA YG LAIN DAN GA KALAH BAGUS DAN MURAH DI ETALASE TOKO KITA\n\nBonus Tasbih Digital Cantik Senilai Rp.20.000 setiap pembelian diatas 250K dan masih banyak bonus lainnya setiap belanja produk dari Olshop Ibansyah\n\n!!! 100% GARANSI JIKA BARANG TIDAK SAMPAI UANG KEMBALI 100% !!!\n\n.\n\nINPUT RESI H 1-2X24 JAM\n\n.\n\nMenjual berbagai macam koko pakistan dan jubah dengan harga sangat murah dan berkualitas , kenapa murah? karena kami langsung produksi sendiri , cocok untuk dijual lg dan bisa untuk dropship dan reseller dan untuk pakaian pribadi\n\nMohon pengertiannya apabila pengiriman lambat dikarena kan stok barang cepat habis karena kita harga grosir\n\nMengenai warna\n\nWarna sesungguhnya mungkin dapat berbeda\n\nHal ini dikarenakan setiap layar memiliki kemampuan yg berbeda beeda untuk menmapilkan warna , kami tidak dapat menjamin bahwa warna yg anda lihat adalah warna akurat dari produk tsb\n\nUntuk Custom Product/Family set/Couple/Dll boleh chat kita untuk Tanya-tanya\n\nMenerima pesanan baju busana muslim segala macam model (pakistan,kurta,jubah/gamis) partai kecil dan besar. (harga menyesuaikan bahan) tergantung keinginan konsumen.', 'Bismillah..\n\nDapatkan bonus merchandise cantik annajah setiap pembelian 1 produk\n\nModel : Kurta\n\nSize : XXS s.d 5XL\n\nMat : Japan Fodu Cotton\n\n\n\n10% keuntungan bersih akan disumbangkan untuk kegiatan sosial\n\n\n\nSize Chart Pakistan/Kurta\n\nXXS : LEBAR DADA 48 , PANJANG TANGAN 50 , PANJANG BADAN 81\n\nXS : LEBAR DADA 50 , PANJANG TANGAN 52 , PANJANG BADAN 84\n\nS : LEBAR DADA 52 , PANJANG TANGAN 54 , PANJANG BADAN 87\n\nM : LEBAR DADA 54 , PANJANG TANGAN 56 , PANJANG BADAN 87\n\nL : LEBAR DADA 56 , PANJANG TANGAN 58 , PANJANG BADAN 90\n\nXL : LEBAR DADA 58 , PANJANG TANGAN 60 , PANJANG BADAN 93\n\nXXL : LEBAR DADA 60 , PANJANG TANGAN 62 , PANJANG BADAN 96\n\n3XL : LEBAR DADA 62 , PANJANG TANGAN 64 , PANJANG BADAN 99\n\n4XL : LEBAR DADA 64 , PANJANG TANGAN 66 , PANJANG BADAN 102\n\n5XL : LEBAR DADA 66 , PANJANG TANGAN 68 , PANJANG BADAN 105\n\n\n\n\n9 Alasan kenapa anda wajib harus memakai produk kami karena kami memberikan banyak keuntungan\n\n\n\n1/ Dipilih dari bahan yg paling baik sehingga memberikan kenyamanan sesungguhnya\n\n2/ Garansi retur ukuran jika kebesaran\n\n3/ Garansi uang kembali jika barang tidak sampai\n\n4/ Model terbaru\n\n5/ Produk Terlaris\n\n6/ Desain terupdate sehingga bisa digunakan untuk kerja kantoran ataupun acara formal dan santai\n\n7/ Bahan adem sehingga cocok untuk digunakan didaerah tropis seperti Indonesia terutama di kota-kota dgn suhu cuaca yg panas\n\n8/ Elegant,Simple,Modern\n\n9/ Berorientasi pd saticfactionoriented tanpa melupakan etika berpakaian\n\n\n\nBerminat? Apalagi yg anda tunggu? Buktikan sendiri karena stok terbatas atau anda akan menyesal', '2023-02-20 03:37:36', '2023-02-20 03:37:36');
INSERT INTO `product_details` VALUES (2, 2, '#pakaian wanita, #pakaian', '-', 'Long John Wanita 03\nPakaian Musim Dingin / Thermal\nAtasan (Baju) saja\n\nReady di Indonesia Tanpa Po\nModel : Turtleneck\n\nKUALITAS PREMIUM BAHAN TEBAL dan HALUS\nbagian luar dan dalam bahan bulu2 halus penahan dingin.\nCocok untuk musim dingin ( salju ) , ke Puncak dan kegiatan outdoor lainnya\n\n*Dijamin Produk berkualitas dan keren\nApa yang anda lihat, itu yang akan anda dapatkan\n\n1. Ungu : L\n2. Hijau : L . XL . 2XL\n\nSaran Ukuran BERAT BADAN :\nSize L : BB 45-50 kg\nSize XL: BB 51-55 kg\nSize 2XL: BB 56- 62 kg\n\n\nNOTE:\nTulisan di bajunya aj size besar ,tapi ukuran baju nya Standar ukuran Berat Badan yang di sarankan ya kak\n\n- Bisa menanyakan kecocokan ukuran dengan memberitahu tinggi dan berat badan\n\nDetail ukuran kurang lebih :\nSize L : Ld 43 up to 58cm X P 61cm\nSize XL: Ld 44 up to 60cm X P 64cm\nSize 2XL: Ld 45 up to 62cm X P 65cm\nSize 3XL : Ld 49 up to 65cm X L 67cm\n\nPENTING !!!\n*MOHON CANTUMKAN PILIHAN SIZE DAN WARNA SAAT ORDER*\n\n- Bila tidak TULIS CATATAN UNTUK TOKO / Bila pilih Size dan Warna yg tidak ada, akan dikirim random sesuai stok yang ada . No Complain\n\nJangan Lupa Bintang 5 nya :)\n\n\nTurle neck rajut wanita | switer | sweater tebal | sweater turtleneck wanita | sweater rajut wanita | sweter | sweater turtle neck pria | sweter rajut wanita | sweeter wanita | turleneck | sweater rajut premium | suwiter wanita | sweeter wanita | sweeter | switer rajut wanita | switer wanita | sweater turtleneck | sweter wanita | sweter rajut | sweeter rajut | sweater rajut | switer rajut | turtle neck wanita | sweater wanita | sweater | sweater import | sweater murah | switer murah | outwear | atasan wanita | outerwear | winter', '2023-02-20 03:04:42', '2023-02-20 03:04:42');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_store` int NOT NULL,
  `id_category` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stocks` int NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `third_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `price` int NOT NULL,
  `discount` int NOT NULL,
  `real_price` int NOT NULL,
  `size` int NOT NULL COMMENT '1 or 0',
  `variant` int NOT NULL COMMENT '1 or 0',
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 1, 1, 'KURTA PRIA AN NAJAH KGK KOKO PAKISTAN AN NAJAH KGK BUSANA MUSLIM PRIA', 77, 'http://127.0.0.1:8001/assets/upload/product/3EIkk1-1676913222.png', 'http://127.0.0.1:8001/assets/upload/product/KR9Gvr-1676913222.png', 'http://127.0.0.1:8001/assets/upload/product/AdsgTs-1676913222.png', 1, 0, 1, 1, 1, 'kurta_pria_an_najah_kgk_koko_pakistan_an_najah_kgk_busana_muslim_pria', 10, '2023-02-20 03:37:36', '2023-02-22 04:19:36');
INSERT INTO `products` VALUES (2, 1, 2, 'LONGJOHN WANITA BAJU PAKAIAN WINTER MUSIM TURTLENECK WANITA CEWEK CEWE', 15, 'http://127.0.0.1:8001/assets/upload/product/pcrfbg-1676913306.png', 'http://127.0.0.1:8001/assets/upload/product/7dVUV8-1676913306.png', 'http://127.0.0.1:8001/assets/upload/product/M6HwRB-1676913306.png', 1, 0, 1, 1, 1, 'longjohn_wanita_baju_pakaian_winter_musim_turtleneck_wanita_cewek_cewe', 10, '2023-02-20 03:04:42', '2023-02-21 00:15:06');

-- ----------------------------
-- Table structure for promos
-- ----------------------------
DROP TABLE IF EXISTS `promos`;
CREATE TABLE `promos`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_product` int NOT NULL,
  `expired_at` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of promos
-- ----------------------------

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` int NOT NULL,
  `review` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reviews
-- ----------------------------
INSERT INTO `reviews` VALUES (1, 1, 'IBAN', 5, 'Bagus', '2023-02-21 00:43:35', '2023-02-21 00:43:35');
INSERT INTO `reviews` VALUES (2, 2, 'IBAN', 5, 'Bagus', '2023-02-21 00:44:15', '2023-02-21 00:44:15');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sessions_user_id_index`(`user_id` ASC) USING BTREE,
  INDEX `sessions_last_activity_index`(`last_activity` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sessions
-- ----------------------------

-- ----------------------------
-- Table structure for size_products
-- ----------------------------
DROP TABLE IF EXISTS `size_products`;
CREATE TABLE `size_products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of size_products
-- ----------------------------
INSERT INTO `size_products` VALUES (1, 1, 'XXS, XS, S, M, L, XL', '2023-02-20 03:37:36', '2023-02-20 03:37:36');
INSERT INTO `size_products` VALUES (2, 2, 'XXS, XS, S, M, L, XL', '2023-02-20 03:04:42', '2023-02-20 03:04:42');

-- ----------------------------
-- Table structure for store_details
-- ----------------------------
DROP TABLE IF EXISTS `store_details`;
CREATE TABLE `store_details`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_store` int NOT NULL,
  `subject_store` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description_store` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of store_details
-- ----------------------------
INSERT INTO `store_details` VALUES (1, 1, 'Toko Serba Ada', 'Menjual Apa aja, harga murah', '2023-02-20 06:24:45', '2023-02-20 06:26:44');

-- ----------------------------
-- Table structure for stores
-- ----------------------------
DROP TABLE IF EXISTS `stores`;
CREATE TABLE `stores`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stores
-- ----------------------------
INSERT INTO `stores` VALUES (1, 1, 'OLSHOP IBANSYAH', 'http://127.0.0.1:8001/assets/upload/store/Xdxiud-1676849085.png', 'olshop_ibansyah', 10, '2023-02-20 06:24:45', '2023-02-20 06:26:44');

-- ----------------------------
-- Table structure for subscribes
-- ----------------------------
DROP TABLE IF EXISTS `subscribes`;
CREATE TABLE `subscribes`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscribes
-- ----------------------------
INSERT INTO `subscribes` VALUES (1, 'ibansyahdien7@gmail.com', '192.168.1.1', '2023-02-20 07:25:28', '2023-02-20 07:25:28');
INSERT INTO `subscribes` VALUES (2, 'ibansyahdienx7@gmail.com', '127.0.0.1', '2023-02-20 22:36:23', '2023-02-20 22:36:23');

-- ----------------------------
-- Table structure for team_invitations
-- ----------------------------
DROP TABLE IF EXISTS `team_invitations`;
CREATE TABLE `team_invitations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `team_id` bigint UNSIGNED NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `team_invitations_team_id_email_unique`(`team_id` ASC, `email` ASC) USING BTREE,
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of team_invitations
-- ----------------------------

-- ----------------------------
-- Table structure for team_user
-- ----------------------------
DROP TABLE IF EXISTS `team_user`;
CREATE TABLE `team_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `team_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `team_user_team_id_user_id_unique`(`team_id` ASC, `user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of team_user
-- ----------------------------

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teams_user_id_index`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teams
-- ----------------------------

-- ----------------------------
-- Table structure for transaction_details
-- ----------------------------
DROP TABLE IF EXISTS `transaction_details`;
CREATE TABLE `transaction_details`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_transaction` int NOT NULL,
  `name_product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_product` int NOT NULL,
  `subtotal` int NOT NULL,
  `discount` int NOT NULL,
  `tax` int NOT NULL,
  `tax_price` int NOT NULL,
  `grandtotal` int NOT NULL,
  `admin_fee` int NOT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaction_details
-- ----------------------------
INSERT INTO `transaction_details` VALUES (1, 1, 'KURTA PRIA AN NAJAH KGK KOKO PAKISTAN AN NAJAH KGK BUSANA MUSLIM PRIA', 1000, 10000, 0, 11, 1100, 11600, 500, 'BANK BRI', '2023-02-22 04:18:28', '2023-02-22 04:18:28');
INSERT INTO `transaction_details` VALUES (2, 2, 'KURTA PRIA AN NAJAH KGK KOKO PAKISTAN AN NAJAH KGK BUSANA MUSLIM PRIA', 1000, 10000, 0, 11, 1100, 11600, 500, 'BANK BRI', '2023-02-22 04:19:36', '2023-02-22 04:19:36');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `id_product` int NOT NULL,
  `id_payment` int NOT NULL,
  `year` int NOT NULL,
  `month` int NOT NULL,
  `day` int NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_transaction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired_due` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (1, 2, 1, 5, 2023, 2, 22, 'pending', 'pending', '2023-02-23 04:18:22', '2023-02-22 04:18:28', '2023-02-22 04:18:28');
INSERT INTO `transactions` VALUES (2, 2, 1, 5, 2023, 2, 22, 'pending', 'pending', '2023-02-23 04:19:30', '2023-02-22 04:19:36', '2023-02-22 04:19:36');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pzn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `two_factor_secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `two_factor_recovery_codes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `current_team_id` bigint UNSIGNED NULL DEFAULT NULL,
  `profile_photo_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'IBAN SYAHDIEN AKBAR', 'ibansyahdienx7@gmail.com', NULL, '$2y$10$Ev7Z0UBzD3/VnsHziguEduDFlvzvuFSgUE4tXU.sgGFrmjSaO6WdC', 'RHRPQjhGT3lvSnoxYUJzOWRtWEdRZz09Ojp7TS7B0p3CxidNwFFb1u16', NULL, NULL, NULL, NULL, NULL, 'https://lh3.googleusercontent.com/a/AEdFTp7YBz0RtlZIy_eF9VpsnPUisws7aCcljPiV85ed3A=s96-c', 'iban_syahdien_akbar', 'superadmin', 10, '2023-02-17 02:35:26', '2023-02-21 03:21:02');
INSERT INTO `users` VALUES (2, 'IBAN', 'ibansyahdien7@gmail.com', '2023-02-20 05:13:30', '$2y$10$ape/13HtGPahiHnAtDUBkeES8P.NP.udPCxDFKYS6wHTGbA/gNi6K', 'VXV5akpPbmhkVzUwelBUNUsya3NnZz09OjpKlaLcp93c8De1l43ZUC6t', NULL, NULL, NULL, NULL, NULL, 'https://lh3.googleusercontent.com/a/AEdFTp5IAOBuA24tcgcs7rMG6TzihD5cqLyIDYLUrhhnrQ=s96-c', 'iban', 'user', 10, '2023-02-20 05:10:24', '2023-02-21 23:13:56');

-- ----------------------------
-- Table structure for va_users
-- ----------------------------
DROP TABLE IF EXISTS `va_users`;
CREATE TABLE `va_users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `va` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of va_users
-- ----------------------------
INSERT INTO `va_users` VALUES (1, 2, '546470009315445549', 'bri', '2023-02-22 04:19:36', '2023-02-22 04:19:36');

-- ----------------------------
-- Table structure for variant_products
-- ----------------------------
DROP TABLE IF EXISTS `variant_products`;
CREATE TABLE `variant_products`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` int NOT NULL,
  `variant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of variant_products
-- ----------------------------
INSERT INTO `variant_products` VALUES (1, 1, 'MAROON, HITAM, BIRU UNGU', '2023-02-20 03:37:36', '2023-02-20 03:37:36');
INSERT INTO `variant_products` VALUES (2, 2, 'MERAH, HITAM, BIRU', '2023-02-20 03:04:42', '2023-02-20 03:04:42');

-- ----------------------------
-- Table structure for wishlists
-- ----------------------------
DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE `wishlists`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `id_product` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wishlists
-- ----------------------------
INSERT INTO `wishlists` VALUES (1, 2, 1, '2023-02-22 00:21:11', '2023-02-22 00:21:11');
INSERT INTO `wishlists` VALUES (2, 2, 2, '2023-02-22 00:21:17', '2023-02-22 00:21:17');

SET FOREIGN_KEY_CHECKS = 1;
